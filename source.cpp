#include <iostream>
#include <vector>

#include "myMatrix.h"

using namespace std;

// сложение матриц
void example1(int rows, int cols, int element) {
	myMatrix<int> m1(rows, cols, element);
	myMatrix<int> m2(rows, cols, element + 10);

	myMatrix<int> result = m1 + m2;

	cout << result << endl;
}

// создание матрицы со значением по умолчанию
void example2(int rows, int cols) {
	myMatrix<int> m1(rows, cols);

	cout << m1 << endl;
}

// умножение матрицы на число
void example3(int rows, int cols) {

	vector<vector<int> > container(rows);
	for (int i = 0; i < rows; i++) {
		container[i] = vector<int>(cols);
		for (int j = 0; j < cols; j++) {
			container[i][j] = j*j + i;
		}
	}
	
	myMatrix<int> m(container);
	cout << m << endl;

	cout << m * 10 << endl;
}

// умножение матриц
void example4(int rows, int cols) {
	vector<vector<int> > container(rows);
	for (int i = 0; i < rows; i++) {
		container[i] = vector<int>(cols);
		for (int j = 0; j < cols; j++) {
			container[i][j] = j*j + i;
		}
	}
	
	myMatrix<int> m(container);

	cout << m << endl << endl;

	cout << m * m << endl;
}

// возведение матрицы в степень
void example5(int rows, int cols) {
	vector<vector<int> > container(rows);
	for (int i = 0; i < rows; i++) {
		container[i] = vector<int>(cols);
		for (int j = 0; j < cols; j++) {
			container[i][j] = j*j + i;
		}
	}
	
	myMatrix<int> m(container);

	cout << m << endl << endl;

	myMatrix<int> result = m^5;
	cout << result << endl;
}

int main() {


	//example1(4, 4, 1);
	//example2(3, 2);
	//example3(4, 5);
	//example4(2, 2);
	example5(2, 2);

	return 0;
}