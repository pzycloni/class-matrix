#pragma once
#include <vector>

using namespace std;

template <typename T> class myMatrix {

    public:
        // здесь будем хранить матрицу
        vector<vector<T > > matrix;

        // конструктор для матрицы
        myMatrix(int rows, int cols) {
            matrix.resize(rows);
            for (int i = 0; i < rows; i++) {
                matrix[i].resize(cols);
            }
        }

        // конструктор для матрицы
        myMatrix(const vector<vector<T > > &_matrix) {
            matrix.resize(_matrix.size());
            for (int row = 0; row < _matrix.size(); row++) {
                matrix[row].resize(_matrix[0].size());
                for (int column = 0; column < _matrix[0].size(); column++) {
                    matrix[row][column] = _matrix[row][column];
                }
            }
        }

        // конструктор для матрицы
        myMatrix() {}

        // конструктор для матрицы
        myMatrix(int rows, int cols, T initElement) {
            matrix.resize(rows);
            for (int i = 0; i < rows; i++) {
                matrix[i].resize(cols);
                for (int j = 0; j < cols; j++) {
                    matrix[i][j] = initElement;
                }
            }

        }

        // получение кол-ва строк в матрице
        inline int getRows() const { return this->matrix.size(); }
        // получение кол-ва колонок в матрице
        inline int getCols() const { return this->matrix.size() ? this->matrix[0].size() : 0; }
        // получение элемента матрицы
        inline T& operator()(int row, int column) { return matrix[row][column]; }

        // присваивание матрицы одной матрицы другой
        myMatrix<T> operator=(const myMatrix<T> &_matrix) {
            if (this == &_matrix)
                return *this;

            if (this->getCols()!= _matrix.getRows() || this->getCols() != _matrix.getCols()) {
                this->matrix.resize(_matrix.matrix.size());
                for (int row = 0; row < _matrix.matrix.size(); row++) {
                    matrix[row].resize(_matrix.matrix[0].size());
                }
            }

            for (int row = 0; row < this->matrix.size(); row++) {
                for (int column = 0; column < this->matrix[0].size(); column++) {
                    this->matrix[row][column] = _matrix.matrix[row][column];
                }
            }

            return *this;
        }

        // сложение матриц
        myMatrix<T> operator+=(const myMatrix<T> &_matrix) {
            for (int row = 0; row < _matrix.matrix.size(); row++) {
                for (int column = 0; column < _matrix.matrix.size(); column++) {
                    matrix[row][column] += _matrix.matrix[row][column];
                }
            }

            return *this;
        }

        // вычитание матриц
        myMatrix<T> operator-=(const myMatrix<T> &_matrix) {
            for (int row = 0; row < _matrix.matrix.size(); row++) {
                for (int column = 0; column < _matrix.matrix.size(); column++) {
                    matrix[row][column] -= _matrix.matrix[row][column];
                }
            }

            return *this;
        }

        // умножение матриц
        myMatrix<T> operator*=(const myMatrix<T> &_matrix) {

            myMatrix<T> result(this->getRows(), _matrix.getCols());

            for (int row = 0; row < result.matrix.size(); row++) {
                for (int column = 0; column < result.matrix[0].size(); column++) {
                    for (int k = 0; k < this->matrix[0].size(); k++) {
                        result.matrix[row][column] = result.matrix[row][column] + this->matrix[row][k] * _matrix.matrix[k][column];
                    }
                }
            }

            this->matrix = result.matrix;

            return *this;
        }

        // умножение матрицы на число
        myMatrix<T> operator*=(T number) {

            for (int row = 0; row < this->getRows(); row++) {
                for (int column = 0; column < this->getCols(); column++) {
                    matrix[row][column] *= number;
                }
            }

            return *this;
        }

        // умножение матриц
        myMatrix<T> operator*(const myMatrix<T> &_b) {
            myMatrix<T> result(this->getRows(), _b.getCols());

            for (int row = 0; row < result.matrix.size(); row++) {
                for (int column = 0; column < result.matrix[0].size(); column++) {
                    for (int k = 0; k < this->matrix[0].size(); k++) {
                        result.matrix[row][column] = result.matrix[row][column] + this->matrix[row][k] * _b.matrix[k][column];
                    }
                }
            }

            return result;
        }

        // деление матрицы на число
        myMatrix<T> operator/=(T number) {

            for (int row = 0; row < this->getRows(); row++) {
                for (int column = 0; column < this->getCols(); column++) {
                    matrix[row][column] /= number;
                }
            }

            return *this;
        }

        // возведение матрицы в степень
        myMatrix<T> operator^(int power) {

            myMatrix<T> m(this->matrix);
            myMatrix<T> result(this->matrix);
            for (int i = 0; i < power - 1; i++) {
                result = m * result;
            }

            return result;
        }
};

// сложение матриц
template<typename T>
myMatrix<T> operator+(const myMatrix<T> &_a, const myMatrix<T> &_b) {
    myMatrix<T> result(_a);
    return (result += _b);
}

// вычитание матриц
template<typename T>
myMatrix<T> operator-(const myMatrix<T> &_a, const myMatrix<T> &_b) {
    myMatrix<T> result(_a);
    return (result -= _b);
}

// перемножение матриц
template<typename T>
myMatrix<T> operator*(const myMatrix<T> &_a, const myMatrix<T> &_b) {
    myMatrix<T> result(_a.getRows(), _b.getCols());

    for (int row = 0; row < result.matrix.size(); row++) {
        for (int column = 0; column < result.matrix[0].size(); column++) {
            for (int k = 0; k < _a.matrix[0].size(); k++) {
                result.matrix[row][column] = result.matrix[row][column] + _a.matrix[row][k] * _b.matrix[k][column];
            }
        }
    }

    return result;
}

// умножение матрицы на число
template<typename T>
myMatrix<T> operator*(const myMatrix<T> &_a, T number) {
    myMatrix<T> result(_a);
    return (result *= number);
}

// деление матрицы на число
template<typename T>
myMatrix<T> operator/(const myMatrix<T> &_a, T number) {
    myMatrix<T> result(_a);
    return (result /= number);
}

// вывод матрицы
template<typename T>
ostream& operator<<(ostream &out, const myMatrix<T> &_matrix) {

    if (!_matrix.getRows()) {
        out << "Matrix is empty!";
    }

    for (int row = 0; row < _matrix.getRows(); row++) {
        for (int column = 0; column < _matrix.getCols(); column++) {
            out << " " << _matrix.matrix[row][column];
        }
        out << endl;
    }
    return out;
}

template<typename T>
istream& operator>>(istream &in, myMatrix<T> &_matrix) {
    for (int row = 0; row < _matrix.getRows(); row++) {
        for (int column = 0; column < _matrix.getCols(); column++) {
            in >> _matrix.matrix[row][column];
        }
    }
    return in;
}